<?php

namespace Anto\SimpleBlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Anto\SimpleBlogBundle\Entity\Post;
use Anto\SimpleBlogBundle\Form\Type\PostType;
use Anto\SimpleBlogBundle\Form\Type\PostFilterType;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Post controller.
 */
class PostController extends Controller
{
    /**
     * Lists all Post entities.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PostFilterType());
        if (!is_null($response = $this->saveFilter($form, 'post', 'admin_post'))) {
            return $response;
        }
        $qb = $em->getRepository('AntoSimpleBlogBundle:Post')->createQueryBuilder('p');
        $paginator = $this->filter($form, $qb, 'post');

        return $this->render('AntoSimpleBlogBundle:Post:index.html.twig', array(
            'form' => $form->createView(),
            'paginator' => $paginator,
        ));
    }

    /**
     * Finds and displays a Post entity.
     */
    public function showAction(Post $post)
    {
        $deleteForm = $this->createDeleteForm($post->getId(), 'admin_post_delete');

        return $this->render('AntoSimpleBlogBundle:Post:show.html.twig', array(
            'post' => $post,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Post entity.
     */
    public function newAction()
    {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);

        return $this->render('AntoSimpleBlogBundle:Post:new.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Post entity.
     */
    public function createAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_post_show', array('id' => $post->getId())));
        }

        return $this->render('AntoSimpleBlogBundle:Post:new.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Post entity.
     */
    public function editAction(Post $post)
    {
        $editForm = $this->createForm(new PostType(), $post, array(
            'action' => $this->generateUrl('admin_post_update', array('id' => $post->getId())),
            'method' => 'PUT',
        ));
        $deleteForm = $this->createDeleteForm($post->getId(), 'admin_post_delete');

        return $this->render('AntoSimpleBlogBundle:Post:edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Post entity.
     */
    public function updateAction(Post $post, Request $request)
    {
        $editForm = $this->createForm(new PostType(), $post, array(
            'action' => $this->generateUrl('admin_post_update', array('id' => $post->getId())),
            'method' => 'PUT',
        ));
        if ($editForm->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('admin_post_edit', array('id' => $post->getId())));
        }
        $deleteForm = $this->createDeleteForm($post->getId(), 'admin_post_delete');

        return $this->render('AntoSimpleBlogBundle:Post:edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Save order.
     */
    public function sortAction($field, $type)
    {
        $this->setOrder('post', $field, $type);

        return $this->redirect($this->generateUrl('admin_post'));
    }

    /**
     * @param string $name  session name
     * @param string $field field name
     * @param string $type  sort type ("ASC"/"DESC")
     */
    protected function setOrder($name, $field, $type = 'ASC')
    {
        $this->getRequest()->getSession()->set('sort.'.$name, array('field' => $field, 'type' => $type));
    }

    /**
     * @param string $name
     *
     * @return array
     */
    protected function getOrder($name)
    {
        $session = $this->getRequest()->getSession();

        return $session->has('sort.'.$name) ? $session->get('sort.'.$name) : null;
    }

    /**
     * @param QueryBuilder $qb
     * @param string       $name
     */
    protected function addQueryBuilderSort(QueryBuilder $qb, $name)
    {
        $alias = current($qb->getDQLPart('from'))->getAlias();
        if (is_array($order = $this->getOrder($name))) {
            $qb->orderBy($alias.'.'.$order['field'], $order['type']);
        }
    }

    /**
     * Save filters.
     *
     * @param FormInterface $form
     * @param string        $name   route/entity name
     * @param string        $route  route name, if different from entity name
     * @param array         $params possible route parameters
     *
     * @return Response
     */
    protected function saveFilter(FormInterface $form, $name, $route = null, array $params = null)
    {
        $request = $this->getRequest();
        $url = $this->generateUrl($route ?: $name, is_null($params) ? array() : $params);
        if ($request->query->has('submit-filter') && $form->handleRequest($request)->isValid()) {
            $request->getSession()->set('filter.'.$name, $request->query->get($form->getName()));

            return $this->redirect($url);
        } elseif ($request->query->has('reset-filter')) {
            $request->getSession()->set('filter.'.$name, null);

            return $this->redirect($url);
        }
    }

    /**
     * Filter form.
     *
     * @param FormInterface $form
     * @param QueryBuilder  $qb
     * @param string        $name
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    protected function filter(FormInterface $form, QueryBuilder $qb, $name)
    {
        if (!is_null($values = $this->getFilter($name))) {
            if ($form->submit($values)->isValid()) {
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $qb);
            }
        }

        // possible sorting
        $this->addQueryBuilderSort($qb, $name);

        return $this->get('knp_paginator')->paginate($qb, $this->getRequest()->query->get('page', 1), 20);
    }

    /**
     * Get filters from session.
     *
     * @param string $name
     *
     * @return array
     */
    protected function getFilter($name)
    {
        return $this->getRequest()->getSession()->get('filter.'.$name);
    }

    /**
     * Deletes a Post entity.
     */
    public function deleteAction(Post $post, Request $request)
    {
        $form = $this->createDeleteForm($post->getId(), 'admin_post_delete');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_post'));
    }

    /**
     * Create Delete form.
     *
     * @param int    $id
     * @param string $route
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function createDeleteForm($id, $route)
    {
        return $this->createFormBuilder(null, array('attr' => array('id' => 'delete')))
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Lists all Post entities.
     */
    public function indexFrontendAction()
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PostFilterType());
        if (!is_null($response = $this->saveFilter($form, 'post', 'admin_post'))) {
            return $response;
        }
        $qb = $em->getRepository('AntoSimpleBlogBundle:Post')->createQueryBuilder('p');
        $paginator = $this->filter($form, $qb, 'post');

        return $this->render('AntoSimpleBlogBundle:Post:indexFrontend.html.twig', array(
            'form' => $form->createView(),
            'paginator' => $paginator,
        ));
    }

    /**
     * Finds and displays a Post entity.
     */
    public function showFrontendAction(Post $post)
    {
        $deleteForm = $this->createDeleteForm($post->getId(), 'admin_post_delete');

        return $this->render('AntoSimpleBlogBundle:Post:showFrontend.html.twig', array(
            'post' => $post,
            'delete_form' => $deleteForm->createView(),        ));
    }
}
