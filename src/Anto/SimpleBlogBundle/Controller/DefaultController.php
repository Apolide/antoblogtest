<?php

namespace Anto\SimpleBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AntoSimpleBlogBundle:Default:index.html.twig');
    }

    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);

        // Redirect User to posts list
        return new RedirectResponse($this->generateUrl('anto_simple_blog_admin_logout'));
    }
}
