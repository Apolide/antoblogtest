<?php

namespace Anto\SimpleBlogBundle\Tests\Helper;

use Anto\SimpleBlogBundle\Helper\ImageScale;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ImageScaleTest extends WebTestCase
{
    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Original Image size are not valid integers!
     */
    public function testWrongOriginalImageSizeException()
    {
        // try to scale image with string parameters
        try {
            $imgScale = new ImageScale('a', 'b');
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Original Image size are not valid integers!', $e->getMessage());
        }

        // try to scale image with on int and one string
        try {
            $imgScale = new ImageScale(1024, 'b');
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Original Image size are not valid integers!', $e->getMessage());
        }
    }

    public function testWrongBoxSizeMissingException()
    {
        // try to create an image scale object
        $imgScale = new ImageScale(1024, 768);

        // get scaling aspect ratio but without box dimensions
        $scalingAspectRatio = $imgScale->getImageShrinkAspectRatio();

        // no scale has to be done since no box is given
        $this->assertEquals(false, $scalingAspectRatio);
    }

    public function testWrongBoxSizeWrongException()
    {
        // try to create an image scale object
        $imgScale = new ImageScale(1024, 768);

        // get scaling aspect ratio but without right box dimensions
        $scalingAspectRatio = $imgScale->getImageShrinkAspectRatio(180, 'aaa');

        // no scale has to be done since no box is given
        $this->assertEquals(false, $scalingAspectRatio);
    }

    public function testHorizontalScalingResult()
    {
        // try to create an image scale object
        $imgScale = new ImageScale(1024, 768);

        // get scaling aspect ratio
        $scalingAspectRatio = $imgScale->getImageShrinkAspectRatio(180, 250);

        $this->assertEquals('0.17578125', $scalingAspectRatio);
    }

    public function testVerticalScalingResult()
    {
        // try to create an image scale object
        $imgScale = new ImageScale(500, 900);

        // get scaling aspect ratio
        $scalingAspectRatio = $imgScale->getImageShrinkAspectRatio(180, 250);

        $this->assertEquals('0.27777777777778', $scalingAspectRatio);
    }

    public function testPreventMagnifyDoNotScale()
    {
        // try to create an image scale object
        $imgScale = new ImageScale(120, 200);

        // get scaling aspect ratio
        $scalingAspectRatio = $imgScale->getImageShrinkAspectRatio(180, 250);

        $this->assertEquals('1.0', $scalingAspectRatio);
    }
}
