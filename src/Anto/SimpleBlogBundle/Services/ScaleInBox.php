<?php

namespace Anto\SimpleBlogBundle\Services;

use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Anto\SimpleBlogBundle\Services\Factory\ThumbnailAbstract;

/**
 * used to get scale ratio.
 */
class ScaleInBox extends ThumbnailAbstract
{
    //protected $resize = 'scale';

    protected $originalImageWidth;
    protected $originalImageHeight;

    public function __construct($originalImageWidth, $originalImageHeight)
    {
        if (!is_int($originalImageWidth) || !is_int($originalImageHeight)) {
            throw new InvalidArgumentException(
                'Original Image size are not valid integers!'
            );
        }
        $this->originalImageWidth = $originalImageWidth;
        $this->originalImageHeight = $originalImageHeight;
    }

    /**
     * Return Scaling Image Aspect Ratio to fit into Thumbnail Box.
     *
     * @param int $originalHeight ( new box height )
     * @param int $originalWidth  ( new box width )
     */
    public function getImageShrinkAspectRatio($boxWidth = null, $boxHeight = null)
    {

        // If target thumb box size are not set it is not possible to have the right aspect ratio
        if (!is_int($boxWidth) || !is_int($boxHeight)) {
            return false;
        }

        $scalingAspectRatio = min(($boxWidth / $this->originalImageWidth), ($boxHeight / $this->originalImageHeight));

        // if the source sizes are smaller than the result thumbnail size,
        // don't resize -- add a margin instead
        // (it is not suggested to magnify images)

        if ($scalingAspectRatio > 1) {
            $scalingAspectRatio = 1.0;
        }

        return $scalingAspectRatio;
    }

    // metodo scale con return
}
