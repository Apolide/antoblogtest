<?php

namespace Anto\SimpleBlogBundle\Services\Factory;

/**
 * All resize type should extend this abstract thumbnail class.
 */
abstract class ThumbnailAbstract
{
    protected $resize;

    public function getAspectRatio()
    {
        return $this->species;
    }
}
