<?php

namespace Anto\SimpleBlogBundle\Services\Factory;

use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Anto\SimpleBlogBundle\Services\ScaleInBox;
use Anto\SimpleBlogBundle\Services\CropInBox;

/**
 * The is the factory which creates thumb objects.
 */
class ThumbnailFactory
{
    protected $originalImageWidth;
    protected $originalImageHeight;
    protected $type;

    public static function factory($type, $originalImageWidth, $originalImageHeight)
    {
        switch ($type) {
            case 'scale':
            $obj = new ScaleInBox($originalImageWidth, $originalImageHeight);
            break;
            case 'crop':
            $obj = new CropInBox($originalImageWidth, $originalImageHeight);
            break;
            default:
                throw new InvalidArgumentException(
                    'Original Image size are not valid integers!'
                );
            throw new InvalidArgumentException('Thumbnail factory could not create the resize type '.$type);
        }

        return $obj;
    }
}
