Simple Blog Bundle Challenge
============================

A small blog on the basis of symfony2 php framework.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

## FEATURES REQUESTED ##

### LOGIN to administrate the articles ###

  * use username (admin) and password (admin) to access the backend (/admin). The data must be validated when being sent.

  * After having successfully logged in the user can create, edit and delete articles.

  * When being logged in the user can delete an article when clicking a button.

  * TODO: It is necessary to pass from http_basic authentication to a custom user provider (http://symfony.com/doc/current/cookbook/security/custom_provider.html) and custom login/logout form or use FOSUserBundle instead.

### EDIT ARTICLES ###

  * When being logged in the user can edit articles when clicking a button.

### NEW ARTICLES ###

  * The logged in user can create new articles, with mandatory and validated fields.

### DELETE ARTICLES ###

  * User can delete an article when clicking a respective button.

### LIST OF ARTICLES ###

  * All articles are displayed in a list view, paginated.
  * To articles list you can apply sorting, filters, search.

### VIEW ARTICLE ###

  * via detail of a single article.



## INSTALLATION BASICS ##

1) enter the console and clone git project folder
-------------------------------------------------

    mkdir /path/to/www
    cd /path/to/www

    git clone git@bitbucket.org:Apolide/antoblogtest.git

2) download composer
--------------------

    cd /path/to/www/antoblogtest
    curl -sS https://getcomposer.org/installer | php


4) run composer install to install project
------------------------------------------

    ./composer.phar install

5) configure your local /etc/hosts file to browse the project
-------------------------------------------------------------
 
    127.0.0.1    lo.antoblogtest.com

6) configure apache virtualhost and restart server
--------------------------------------------------

    <VirtualHost *:80>
            ServerName lo.antoblogtest.com
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/antoblogtest/web
    
    	AddCharset UTF-8 .utf8
    	AddDefaultCharset UTF-8
    
            <Directory "/var/www/antoblogtest/web">
                    AllowOverride All
    		Order allow,deny
    		Allow from All
            </Directory>
    </VirtualHost>

7) try open in browser http://lo.antoblogtest.com, there could be some errors to fix (usually cache folder permissions is one of them)
--------------------------------------------------------------------------------------------------------------------------------------

    HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
    sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs

8) check server and symfony configuration
-----------------------------------------

    http://lo.simpleblogtest.com/config.php (solve eventual problems)

9) Done. Now you can launch tests to check that is all right
------------------------------------------------------------

    phpunit -c app


10) Create fake post data to browse the backend (/admin/post) and the frontend (/post)
--------------------------------------------------------------------------------------

    app/console faker:populate


## PROJECT SCREENSHOTS ##

![1_antoblogtest_fe_home_mini.png](https://bitbucket.org/repo/4r4g77/images/2757092695-1_antoblogtest_fe_home_mini.png)
Frontend Home Page
------------------

![2_antoblogtest_fe_display_mini.png](https://bitbucket.org/repo/4r4g77/images/2175495761-2_antoblogtest_fe_display_mini.png)
Frontend Display Post Page


![3_antoblogtest_be_list_mini.png](https://bitbucket.org/repo/4r4g77/images/4101944425-3_antoblogtest_be_list_mini.png)
Backend List Posts


![4_antoblogtest_be_createnew_mini.png](https://bitbucket.org/repo/4r4g77/images/1442247216-4_antoblogtest_be_createnew_mini.png)
Backend Create New Post


![5_antoblogtest_be_edito_delete_mini.png](https://bitbucket.org/repo/4r4g77/images/3592845550-5_antoblogtest_be_edito_delete_mini.png)
Backend Edit/Delete Post